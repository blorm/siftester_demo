package pkg;

import com.siperian.sif.client.SiperianClient;
import com.siperian.sif.message.RecordKey;
import com.siperian.sif.message.SiperianObjectType;
import com.siperian.sif.message.mrm.GetRequest;
import com.siperian.sif.message.mrm.GetResponse;

import javax.naming.Context;
import java.util.Properties;

public class Main {

    private static final int EJB = 1;
    private static final int HTTP = 2;
    private static final int SOAP = 3;

    private static final int PROTOCOL = EJB;

    public static void main(String[] args) throws Exception {
        final Properties config = new Properties();

        config.put("siperian-client.orsId", "localhost-orcl-CMX_ORS");
        config.put("siperian-client.username", "admin");
        config.put("siperian-client.password", "admin");

        if (PROTOCOL == HTTP) {
            config.put("siperian-client.protocol", "http");
            config.put("http.call.url", "http://192.168.56.101:8080/sifproxy/request");
        } else if (PROTOCOL == SOAP) {
            config.put("siperian-client.protocol", "soap");
            config.put("soap.call.url", "http://192.168.56.101:8080/sifproxy/services/SifService");
        } else {
            config.put("siperian-client.protocol", "ejb");
            config.put(Context.INITIAL_CONTEXT_FACTORY, "com.globalss.jndiproxy.InitialContextFactoryImpl");
            config.put("delegate.naming.factory.initial", "org.jboss.naming.remote.client.InitialContextFactory");
            config.put(Context.PROVIDER_URL, "remote://192.168.56.101:4447");
            config.put("jboss.naming.client.ejb.context", "true");
            //config.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
            //config.put(Context.SECURITY_PRINCIPAL, "ejbuser");
            //config.put(Context.SECURITY_CREDENTIALS, "ejbuser123!");
        }

        SiperianClient siperianClient = SiperianClient.newSiperianClient(config);

        GetRequest request = new GetRequest();
        request.setRecordKey(RecordKey.rowid("1"));
        request.setSiperianObjectUid(SiperianObjectType.PACKAGE.makeUid("PKG_PRODUCT"));

        GetResponse response = (GetResponse) siperianClient.process(request);

        System.out.println(response.getMessage());
    }
}
